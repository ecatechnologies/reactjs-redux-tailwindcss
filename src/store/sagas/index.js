import { put, takeLatest, all, call } from 'redux-saga/effects'
import webAPI from '../../services'

function* callAPILogin(action) {
  // const json = yield call(
  //   webAPI.Core.login('supakorn.sangroung@gmail.com', 'password')
  // ).then((response) => response)
  // console.log(json)
  // yield put({ type: 'NEWS_RECEIVED', json: json })
  try {
    const response = yield call(
      webAPI.Core.login('supakorn.sangroung@gmail.com', 'password')
    )
  } catch (error) {
    console.log('error = ', error)
  }
}

function* actionWatcher() {
  yield takeLatest('LOGIN_SEND', callAPILogin())
}

function* rootSaga() {
  yield all([actionWatcher()])
}

export default rootSaga
