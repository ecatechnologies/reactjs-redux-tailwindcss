import { Component } from 'react'
import { connect } from 'react-redux'
import { checkLogin } from '../store/actions/index.js'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: null,
      password: null,
    }

    // supakorn.sangroung@gmail.com
    console.log('Login', props)
  }
  loginHandler = async () => {
    console.log('click loginHandler')
    // dispatch({ type: "USER_FETCH_REQUESTED", payload: this.state.username });
  }

  inputHandler = (field, event) => {
    const input = event.target.value
    this.setState({
      [field]: input,
    })
  }

  render() {
    return (
      <>
        <div className="flex h-screen items-center">
          <div className="px-2 lg:px-4 mx-auto max-w-2xl justify-center items-center">
            {/* <loading :active="isLoading" :is-full-page="true"></loading> */}
            <div className="flex flex-col my-3 bg-white rounded-lg shadow-md py-12 mx-auto items-center justify-center space-y-5 w-full px-6 lg:px-16 xl:px-24">
              <div className="w-full py-4 text-xl text-center">
                ระบบบริการจัดการร้านรับจำนำ
              </div>

              <div className="flex flex-col w-full justify-center">
                <span className="py-1 text-base">ชื่อผู้ใช้งาน</span>
                <input
                  type="text"
                  className="font-light flex-1 border border-gray-400 rounded-full w-full px-6 py-2.5 outline-none shadow-md"
                  placeholder="Username"
                  name="username"
                  onChange={(e) => this.inputHandler('username', e)}
                />
              </div>
              {/* v-model="userProfile.username" */}

              <div className="flex flex-col w-full justify-center">
                <span className="py-1 text-base">รหัสผ่าน</span>
                <input
                  type="password"
                  className="font-light flex-1 border border-gray-400 rounded-full w-full px-6 py-2.5 outline-none shadow-md"
                  placeholder="Password"
                  name="password"
                  onChange={(e) => this.inputHandler('password', e)}
                />
              </div>
              {/* v-model="userProfile.password" */}
              {/* onKeyUp="enterSubmit" */}

              <div className="w-full py-3.5">
                <button
                  className="flex py-3 w-full rounded-full justify-center bg-red text-white text-center items-center hover:bg-opacity-80 hover:shadow cursor-pointer shadow-md text-normal"
                  onClick={this.props.checkLogin}>
                  เข้าสู่ระบบ
                </button>
                <div className="py-6">
                  <hr />
                </div>
                <span className="text-sm text-green-500">
                  <i className="fas fa-lock mr-1 text-md"></i>
                  เว็บไซต์นี้ได้เข้ารหัสแบบ SSL 256-bit
                  ข้อมูลของคุณจะได้รับการป้องกัน
                </span>
              </div>
            </div>
            <div className="flex flex-col max-w-2xl mx-auto">
              <span className="text-gray-500 text-sm">เวอร์ชั่น 1.0</span>
            </div>
          </div>
        </div>
      </>
    )
  }
}

const mapDispatchToProps = {
  checkLogin: checkLogin,
}
// // loading: (isLoading) => dispatch(loading(isLoading)),
// const mapStateToProps = (state) => {
//   return {};
// };

export default connect(null, mapDispatchToProps)(Login)
