import axios from "axios";
import { uriCore } from "./../../utils/Constant";

export const registerUserService = (request) => {
  const REGISTER_API_ENDPOINT = "http://localhost:4000/api/v1/register";

  const parameters = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(request.user),
  };

  return fetch(REGISTER_API_ENDPOINT, parameters)
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      return json;
    });
};

const Core = () => {
  // login: (username, password) => {
  //   return axios({
  //     url: `${uriCore}/login`,
  //     method: "POST",
  //     crossdomain: true,
  //     data: {
  //       username: username,
  //       password: password,
  //     },
  //   }).catch((error) => {
  //     if (error.response.status === 401) {
  //       localStorage.removeItem("accessToken");
  //       window.location = "/login";
  //     }
  //   });
  // };
};

export default Core;
